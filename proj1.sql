-- COMP9311 15s1 Project 1
--
-- MyMyUNSW Solution Template

-- Q1: ...

CREATE OR REPLACE VIEW hello(name, cCount) AS
select c.student, count(c.student) as cCount
from course_enrolments c
inner join people on c.student = people.id
group by c.student;


create or replace view Q1(unswid, name)
as
select people.unswid, people.name from hello h
join people on h.name = people.id
where ccount > 65
group by people.unswid, people.name;


-- Q2: ...
CREATE OR REPLACE VIEW nstudents(nstudents) AS
    SELECT count(s.id)
    FROM students s
;

CREATE OR REPLACE VIEW nstaff(nstaff) AS
    SELECT count(t.id)
    FROM staff t
;

CREATE OR REPLACE VIEW nboth(nboth) AS
    select count(s.id) from staff t, students s where s.id = t.id
;


create or replace view Q2(nstudents, nstaff, nboth)
as
SELECT * FROM nstudents, nstaff, nboth
;

-- Q3: ...
CREATE OR REPLACE VIEW licCount(staffId, staffCount)
AS
select s.staff, count(s.staff)
from course_staff s
where s.role = (
    select staff_roles.id
    from staff_roles
    where staff_roles.name = 'Course Convenor'
)
group by s.staff
;


create or replace view Q3(name, ncourses)
as
select people.name, l.staffCount
from licCount l
inner join people on l.staffId = people.id
where l.staffCount = (
    select max(staffCount)
    from licCount
);


-- Q4: ...

CREATE OR REPLACE VIEW degCode(id,name, code) AS
select p.id, p.name, p.code
from programs p
where p.name = 'Computer Science' AND p.code ='3978';

CREATE OR REPLACE VIEW s2(student, program, semester) AS
select e.student, e.program, e.semester from program_enrolments e 
where e.semester = (
select s.id from semesters s
where s.year = '2005' AND s.term = 'S2');


create or replace view Q4a(id)
as
select p.unswid from s2 s, degCode d, people p
where s.program = d.id AND s.student = p.id;


CREATE OR REPLACE VIEW strSeng(id, code) AS
select s.id, s.code from streams s where s.code = 'SENGA1';

CREATE OR REPLACE VIEW test1(student, stream, semester) AS
select program_enrolments.student, e.stream, program_enrolments.semester 
from stream_enrolments e
INNER JOIN program_enrolments ON e.partof = program_enrolments.id
INNER JOIN strSeng ON e.stream = strSeng.id;



create or replace view Q4b(id)
as
select people.unswid from s2 s
inner join test1 on s.student = test1.student and s.semester = test1.semester
inner join people on s.student = people.id;

CREATE OR REPLACE VIEW Q4ci(id, code)
AS
select p.id, p.code
from programs p
where p.offeredby = (
    select o.id                 
    from orgunits o
    where o.unswid = 'COMPSC'
);


CREATE OR REPLACE VIEW Q4cii(studentId)
AS
select s.student
from s2 s
inner join q4ci on s.program = q4ci.id;


create or replace view Q4c(id)
as
select people.unswid
from q4cii p
inner join people on p.studentId = people.id;



-- Q5: ...

CREATE OR REPLACE VIEW aa(utype, name, fac) AS
select o.utype, o.name, facultyOf(o.id)
from orgunits o
where utype = 1 or utype = 2 or utype = 9;

CREATE OR REPLACE VIEW bb( name, count) AS
select orgunits.name, count(a.fac)
from aa a
join orgunits on a.fac = orgunits.id
group by a.fac, orgunits.name;


create or replace view Q5(name)
as
select b.name
from bb b where b.count = (
select max(count) from bb);

-- Q6: ...

create or replace function Q6(text)
	returns table (course text, year integer, term text, convenor text)
as $$
 select cast(subjects.code as text), semesters.year, cast(semesters.term as text), cast(people.name as text) 
    from courses c
    inner join subjects on c.subject = subjects.id
    join course_staff on c.id = course_staff.course
    join people on course_staff.staff = people.id
    join semesters on c.semester = semesters.id
    where subjects.code = $1 AND course_staff.role = (
        select staff_roles.id from staff_roles
        where staff_roles.name = 'Course Convenor')
$$ language sql
;



