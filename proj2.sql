

create or replace function Q1(integer) returns text
as
$$
select p.name
from people p
where id = $1 OR unswid = $1
$$ language sql
;


                         

create or replace function Q2(integer)
	returns setof newTranscriptRecord
as $$
declare
        rec newTranscriptRecord;
        UOCtotal integer := 0;
        UOCpassed integer := 0;
        wsum integer := 0;
        wam integer := 0;
        x integer;
begin
        select s.id into x
        from   Students s join People p on (s.id = p.id)
        where  p.unswid = $1;
        if (not found) then
                raise EXCEPTION 'Invalid student %',$1;
        end if;
        for rec in
                select su.code,
                         substr(t.year::text,3,2)||lower(t.term),
                         pr.code,
                         substr(su.name,1,20),
                         e.mark, e.grade, su.uoc
                from   People p
                         join Students s on (p.id = s.id)
                         join Course_enrolments e on (e.student = s.id)
                         join Courses c on (c.id = e.course)
                         join Subjects su on (c.subject = su.id)
                         join Semesters t on (c.semester = t.id)
			 join program_enrolments pe on (s.id = pe.student and t.id  = pe.semester)
			 join programs pr on pe.program = pr.id
                where  p.unswid = $1
                order  by t.starting, su.code
                loop
                        if (rec.grade = 'SY') then
                                UOCpassed := UOCpassed + rec.uoc;
                        elsif (rec.mark is not null) then
                                if (rec.grade in ('PT','PC','PS','CR','DN','HD','A','B','C')) then
                                        -- only counts towards creditted UOC
                                        -- if they passed the course
                                        UOCpassed := UOCpassed + rec.uoc;
                                end if;
                                -- we count fails towards the WAM calculation
                                UOCtotal := UOCtotal + rec.uoc;
                                -- weighted sum based on mark and uoc for course
                                wsum := wsum + (rec.mark * rec.uoc);
                                -- don't give UOC if they failed
                                if (rec.grade not in ('PT','PC','PS','CR','DN','HD','A','B','C')) then
                                        rec.uoc := 0;
                                end if;

                        end if;
                        return next rec;
                end loop;
                if (UOCtotal = 0) then
                        rec := (null,null, null,'No WAM available',null,null,null);
                else
                        wam := wsum / UOCtotal;
                        rec := (null,null, null, 'Overall WAM',wam,null,UOCpassed);
                end if;
                -- append the last record containing the WAM
                return next rec;
        end;
$$ language plpgsql
;



create or replace function Q3(integer)
		returns setof AcObjRecord
as $$
declare
	_id integer;
	_fullArr text;
	_temp text;
	_temp1 text;
	_temp2 text;
	_end text;
	_counter integer;
	_arr text[];
	_replace text;
	_tempArray text[];
	_innerVal text;
	_low integer;
	_high integer;
	_isEnum text[];
	_gdef text;
	_skipPattern integer;
begin
	select a.gdefby into _gdef
	from acad_object_groups a
	where a.id = $1;
	if (not found) then
                raise EXCEPTION 'Invalid student %',$1;	
	end if;
	select a.definition into _fullArr from acad_object_groups a where a.id = $1;	
    select regexp_split_to_array(_fullArr, '')
	into _isEnum;
	for j in 1..array_upper(_isEnum, 1)
	LOOP
	    if (_isEnum[j] = '{') then
	        _skipPattern = 1;
	        EXIT;
	    else 
	        _skipPattern = 0;
	    end if;    	    
	END LOOP;
	select regexp_split_to_array(_fullArr, ',')
	into _arr;
	select a.gtype into _fullArr
	from acad_object_groups a
	where a.id = $1;
    if (_skipPattern != 1 and _gdef = 'pattern') then
	    for i in 1..array_upper(_arr, 1)
     	LOOP	
	        _temp = replace(_arr[i], '#', '.'); 
		    if (substring(_arr[i], 1, 4) = 'FREE' or substring(_arr[i], 1, 4) = 'GEN#' or
		    substring(_arr[i], 1, 4) = 'GENG' or 
		    substring(_arr[i], 1, 4) = 'ZGEN')        then
		        return next values _fullArr, arr[i]; 
		    elsif (char_length(_temp) = 8) then
			    return query			
			    select _fullArr::text, s.code::text from subjects s 
			    where s.code ~ _temp;
		    elsif (_arr[i] ~ '.+\[.+\]') then
		        		_innerVal = substring(_arr[i], '\[(.*?)\]');
                        _counter = char_length(_temp);
		        		_replace = substring(_arr[i], '\[.+\]');                    
		        	select regexp_split_to_array(_innerVal, '')
	                into _tempArray;
	                if (_tempArray[2] = '-') then 
	                    _low = cast(_tempArray[1] as int);
	                    _high = cast(_tempArray[3] as int);
	                    _counter = _high - _low;
	                	LOOP
	                	    _temp1 = _arr[i];
	                	    _temp1 = replace(_arr[i], _replace, cast(_high-_counter as text));
	                	    _temp1 = replace(_temp1, '#', '.'); 
	                	    if (char_length(_temp1) = 8) then
		                        return query			
		                        select _fullArr::text, s.code::text from subjects s 
		                        where s.code ~ _temp1;
		                    END if;
		                    EXIT WHEN _counter = 0;
		                    _counter = _counter - 1;
	                	END LOOP;
	                elsif (_tempArray[2] != '-') then
	                	for z in 1..array_upper(_tempArray, 1)
	                	LOOP
	                	    _temp1 = _arr[i];
	                	    _temp1 = replace(_arr[i], _replace, _tempArray[z]);
	                	    _temp1 = replace(_temp1, '#', '.');
	                	    if (char_length(_temp1) = 8) then
		                        return query			
		                        select _fullArr::text, s.code::text from subjects s 
		                        where s.code ~ _temp1;
		                    END if;
	                	END LOOP;
	                end if;
		    elsif (substring(arr[i], 1, 16) = '(COMP|SENG|BINF)') then
		        _end = substring(arr[i], 17, 4);
	            _end = replace(_end, '#', '.');
		        _temp = substring(arr[i], 2, 4);
		        _temp1 = substring(arr[i], 7, 4);
		        _temp2 = substring(arr[i], 12, 4);
		        _temp = _temp || _end;
		        _temp1 = _temp1 || _end;
		        _temp2 = _temp2 || _end;
		        if (char_length(_temp) = 8) then
			        return query			
			        select _fullArr::text, s.code::text from subjects s 
			        where s.code ~ _temp;
			        return query			
			        select _fullArr::text, s.code::text from subjects s 
			        where s.code ~ _temp1;
			        return query			
			        select _fullArr::text, s.code::text from subjects s 
			        where s.code ~ _temp2;
			     end if;
		    end if;		
	    END LOOP;
	end if;    				
end;
$$ language plpgsql
;

